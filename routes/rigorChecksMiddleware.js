const { DASHBOARD_ID } = require('../constants');
const _ = require('lodash');
const { getRigorData } = require('../services/httpService');

const groupByDate = data => data.reduce((a, c) => {
  const date = c.timestamp.split('T')[0];
  return Object.assign(a, {
    [date]: a[date] ? [...a[date], c.id] : [c.id]
  })
}, {});

const reduceIds = data => Object.keys(data).reduce((a, c) => {
  return a.concat(_.take(data[c], 5))
}, [])

module.exports = (req, res, next) => {
  const { start, end, metrics } = req.query;
  const query = `/checks/real_browsers/${DASHBOARD_ID}/runs?from=${start}&to=${end}&page=1&per_page=1000`;

  getRigorData(query).then(({ statusCode, data }) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');

    const { runs } = data;
    const reducedIds = reduceIds(groupByDate(runs));
    const promises = reducedIds.reduce((acc, id) => {
      const query = `/checks/real_browsers/${DASHBOARD_ID}/runs/${id}`;
      return acc.concat(getRigorData(query));
    }, []);
    Promise.all(promises).then(values => {
      req.resultData = values.reduce((acc, curr) => acc.concat(curr.data), []);;
      next()
    });
  })
}
