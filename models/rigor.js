const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RigorSchema = new Schema({
  queryKey: String,
  data: String
});

module.exports = mongoose.model('Rigor', RigorSchema);
