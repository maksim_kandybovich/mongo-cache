const TARGET_ORIGIN = 'qa3.sephora';

(function() {
  const vm = {
    activeTab: '',
    start: moment().add(-2, 'day'),
    end: moment(),
    isFetching: false
  };

  const populatePoints = (prev, curr) => {
    if (prev) {
      return [...prev.pageRendered, Math.ceil(curr) || 0]
    }

    return [curr.pageRendered || 0]
  }

  const isOriginUrl = url => !/^https:\/\/qa3.sephora.com/g.test(url) ||
    url === 'https://qa3.sephora.com/serverInfo' ||
    url === 'https://qa3.sephora.com/about-us' ||
    url === 'https://qa3.sephora.com/images/favicon.ico'


  const getParameterByName = (name, url) => {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  };

  function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  const getPageType = url => getParameterByName('page-type', url);
  const getEntriesByType = (data = [], type) => {
    const res = data.find(({ name }) => name === type) || 0;
    return Math.round(Number(res.value) || 0);
  };
  const createQuery = data => data.split(',').reduce((acc, curr) => `${acc}metrics[]=${curr}&`, '')
  const sum = (...args) => args.reduce((acc, curr) => acc + (Number(curr) || 0), 0);
  const convertToKb = bytes => Math.round(0.001 * bytes);
  const color = {
    'http://qa3.sephora.com/': 'red',
    'http://qa3.sephora.com/basket': 'blue',
    'http://qa3.sephora.com/new-makeup': 'green',
    'http://qa3.sephora.com/product/facial-treatment-essence-P375849': 'yellow',
    'http://qa3.sephora.com/shop/eye-makeup': 'pink',
    'http://qa3.sephora.com/shop/makeup-cosmetics': 'orange',
    'https://qa3.sephora.com/checkout/shipping': 'black',
    'https://qa3.sephora.com/volition-beauty': 'purple',
  };

  // const getAvgByDay = ({ byDate }) => Object.keys(byDate).reduce((acc, curr) => {
  //   return {
  //     ...acc,
  //     labels: [ ...acc.labels, curr],
  //     points: [ ...acc.points, Math.round(byDate[curr].reduce((a, c = 0) => a + c / byDate[curr].length, 0))]
  //   }
  // }, { points: [], labels: [] });

  const getAvgByDay = (byUrl) => Object.keys(byUrl).reduce((acc, curr) => {
    let points = [];
    let pointForSlice = [];

    const labels = byUrl[curr].labels.reduce((prevPoints, currPoint, index) => {
      const formattedPoint = moment(currPoint).format('YYYY-MM-DD');
      if (formattedPoint === prevPoints[prevPoints.length - 1]) {
        return prevPoints
      }
      pointForSlice.push(index);
      return [...prevPoints, formattedPoint];
    }, []);

    pointForSlice.push(byUrl[curr].labels.length);

    pointForSlice.reduce((prevSlice, currSlice) => {
      points.push(
        Math.round(byUrl[curr].points.slice(prevSlice, currSlice).reduce((a, c = 0) => a + c, 0) / (currSlice - prevSlice))
      );

      return currSlice;
    });

    return {
      ...acc,
      [curr]: {
        labels,
        points
      }
    }
  }, {  })

  const populateDataFromBrowserCheck = data => data.reduce((acc, curr) => {
    const speedIndex = curr.pages[0].speed_index;

    // if (speedIndex >= 4000) {
    //   return acc
    // }

    const pages = curr.pages.reduce((prev, currPage) => {
      return {
        ...prev,
        [currPage.url]: {
          pageRendered: populatePoints(acc.pages[currPage.url], getEntriesByType(currPage.custom_timings, 'Page Rendered'))
        }
      }
    }, {});

    return {
      ...acc,
      labels: [
        ...acc.labels,
        moment(curr.timestamp).format('MMM Do, h:mm a')
      ],
      pages,
      points: [
        ...acc.points,
        speedIndex
      ]
    }
  }, {labels: [] , points: [], pages: {} });

  const populateByUrl = (byUrl, currItem, metric) => {
    let urlParams = new URLSearchParams(currItem.url.split('?')[1]);
    const pageGroup = urlParams.get('page-template') || '';
    return byUrl ?
      {
        labels: [...byUrl.labels, currItem.time],
        points: [...byUrl.points, metric],
        pageGroup: byUrl.pageGroup && pageGroup
      }
    :
      {
        labels: [currItem.time],
        points: [metric],
        pageGroup
      }
  };

  const mergeByUrl = (data, initial) => Object.keys(data).reduce((acc, curr) => {
    const field = data[curr];
    return {
      ...acc,
      [curr]: acc[curr] ?
        {
          labels: [...acc[curr].labels, ...field.labels],
          points: [...acc[curr].points, ...field.points],
          pageGroup: field.pageGroup,
        }
        :
        {
          labels: [...field.labels],
          points: [...field.points],
          pageGroup: field.pageGroup,
        }
    }
  }, initial);

  const populateSpeedIndex = ({ series }) => {
    const isAvgByDay = vm.end.diff(vm.start, 'days') > 3;
    const result = series.reduce((acc, curr) => {
      const data = curr.data.reduce((prev, currItem) => {
        const [ pageUrl, params ] = currItem.url.split('?');

        if (isOriginUrl(pageUrl)) {
          return prev;
        }

        const urlParams = new URLSearchParams(params);
        const pageGroup = urlParams.get('page-template') || '';
        const url = pageGroup || pageUrl;

        return {
          ...prev,
          byUrl: {
            ...prev.byUrl,
            [url]: populateByUrl(
              prev.byUrl[url],
              currItem,
              currItem.speed_index
            )
          }
        }
      }, { byUrl: {} })

      return mergeByUrl(data.byUrl, acc)

    }, {});

    return isAvgByDay ? getAvgByDay(sortByDate(result)) : sortByDate(result)
  }

  const populatePageSize = ({ series }) => {
    const isAvgByDay = vm.end.diff(vm.start, 'days') > 30;
    const result = series.reduce((acc, curr) => {
      let data = curr.data.reduce((prev, currItem) => {
        if (convertToKb(currItem.html_bytes) < 5) {
          return prev
        }

        const [ pageUrl, params ] = currItem.url.split('?');

        if (isOriginUrl(pageUrl)) {
          return prev;
        }

        const urlParams = new URLSearchParams(params);
        const pageGroup = urlParams.get('page-template') || '';
        const url = pageGroup || pageUrl;

        return {
          ...prev,
          byUrl: {
            ...prev.byUrl,
            [url]: populateByUrl(
              prev.byUrl[url],
              currItem,
              convertToKb(sum(currItem.html_bytes))
            )
          }
        }

      }, { byUrl: {} });

      return mergeByUrl(data.byUrl, acc)

    }, {});

    return isAvgByDay ? getAvgByDay(sortByDate(result)) : sortByDate(result)
  }

  function compareDates(left, right) {
    return moment(right).diff(moment(left), 'minutes') < 0;
  }

  function sortByDate(data) {
    return Object.keys(data).reduce((acc, curr) => {
      const points = [...data[curr].points];
      const labels = [...data[curr].labels];

      const labelsLength = labels.length;
      for(let i = 0; i < labelsLength; i++) {
        let key = labels[i];
        let pointsKey = points[i];
        let j = i - 1;

        while(j >= 0 && compareDates(labels[j], key)) {
          labels[j + 1] = labels[j];
          points[j + 1] = points[j];
          j = j - 1;
        }

        labels[j + 1] = key;
        points[j + 1] = pointsKey;
      }

      return {
        ...acc,
        [curr]: {
          ...data[curr],
          points,
          labels
        }
      }
    }, {})
  }

  const populatePageRender = ({ series }) => {
    const isAvgByDay = vm.end.diff(vm.start, 'days') > 3;
    const result = series.reduce((acc, curr) => {

      let data = curr.data.reduce((prev, currItem) => {
        const [ pageUrl, params ] = currItem.url.split('?');

        if (isOriginUrl(pageUrl)) {
          return prev;
        }

        const urlParams = new URLSearchParams(params);
        const pageGroup = urlParams.get('page-template') || '';
        const url = pageGroup || pageUrl;

        return {
          ...prev,
          byUrl: {
            ...prev.byUrl,
            [url]: populateByUrl(
              prev.byUrl[url],
              currItem,
              getEntriesByType(currItem.custom_timings, 'Page Rendered')
            )
          }
        }

      }, { byUrl: {} });

      return mergeByUrl(data.byUrl, acc)

    }, {});

    return isAvgByDay ? getAvgByDay(sortByDate(result)) : sortByDate(result)
  }

  const getBundleData = (prevData, data, bundleName) => {
    const response = data.log.entries.find(entry => entry.request.url.indexOf(bundleName) !== -1);
    if(!response) {
      return prevData[bundleName] || {};
    }
    const points = convertToKb(response.response.bodySize);
    const labels = moment(data.log.pages[0].startedDateTime).format('YYYY-MM-DD');
    return {
      url: data.log.pages[0].title,
      points: prevData[bundleName] && prevData[bundleName].points ? prevData[bundleName].points.concat(points) : [points],
      labels: prevData[bundleName] && prevData[bundleName].labels ? prevData[bundleName].labels.concat(labels) : [labels],
    }
  }

  const COMPONENTS_BUNDLE = 'components.chunk';
  const POSTLOAD_BUNDLE = 'postload.chunk';
  const PRIORITY_BUNDLE = 'priority.bundle';

  const populateBundleSize = data => data.reduce((a, c) => {
    if( c.log.entries.length < 10) {
      return a;
    }

    return {
      ...a,
      [COMPONENTS_BUNDLE]: getBundleData(a, c, COMPONENTS_BUNDLE),
      [POSTLOAD_BUNDLE]: getBundleData(a, c, POSTLOAD_BUNDLE),
      [PRIORITY_BUNDLE]: getBundleData(a, c, PRIORITY_BUNDLE),
    }
  }, {})

  const createChart = (pageUrl, data) => {
    const wrapper = document.createElement('div');
    const id = `${pageUrl}_${vm.activeTab}`;
    wrapper.className = 'chart-wrap';
    wrapper.innerHTML = `
    <h2 class="chart-header">${data[pageUrl].pageGroup || pageUrl}</h2>
    <div><canvas id=${id} width="400" height="100"></canvas></div>`;

    const chartsContainer = document.getElementById(vm.activeTab);
    chartsContainer.appendChild(wrapper);
    activeCharts.push(
      new Chart(document.getElementById(id), {
        type: 'line',
        data: {
          labels: data[pageUrl].labels,
          datasets: [{
            label: pageUrl,
            data: data[pageUrl].points,
            backgroundColor: [
              'rgba(35, 3, 106, 0.2)',
            ],
            borderColor: [
              'rgba(35, 3, 106, 1)',
            ],
            borderWidth: 2,
            pointHitRadius: 5,
            pointBackgroundColor: 'rgba(0, 84, 87, 0)',
            pointBorderColor: 'rgba(0, 84, 87, 0)',
            pointHoverBackgroundColor: 'rgba(0, 84, 87, 1)',
            cubicInterpolationMode: 'monotone',
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true
              }
            }]
          }
        }
      })
    )
  }

  const createPageRenderChart = (data) => {
    const wrapper = document.createElement('div');
    const id = `$page-render_${vm.activeTab}`;
    wrapper.className = 'chart-wrap--full';
    wrapper.innerHTML = `
    <h2 class="chart-header">Page render</h2>
    <div><canvas id=${id} width="400" height="200"></canvas></div>`;

    const chartsContainer = document.getElementById(vm.activeTab);
    chartsContainer.appendChild(wrapper);

    let labels = null;
    const datasets = Object.keys(data).reduce((acc, curr) => {

      if (!labels) {
        labels = data[curr].labels
      }
      return [
        ...acc,
        {
          label: `${data[curr].pageGroup || curr}`,
          data: data[curr].points,
          borderColor: [getRandomColor()],
          backgroundColor: [
            'rgba(255, 99, 132, 0)',
          ],
          borderWidth: 2,
          pointHitRadius: 10,
          pointBorderColor: 'rgba(0, 84, 87, 0)',
          pointHoverBackgroundColor: color[curr],
          cubicInterpolationMode: 'monotone'
        }
      ]
    }, [])
    activeCharts.push(
      new Chart(document.getElementById(id), {
        type: 'line',
        data: {
          labels,
          datasets
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true
              }
            }]
          }
        },
        spanGaps: true
      })
    )
  }

  const bundlesColors = {
    [POSTLOAD_BUNDLE]: 'red',
    [PRIORITY_BUNDLE]: 'blue',
    [COMPONENTS_BUNDLE]: 'green',
  };

  const createBundleSizeChart = (data) => {
    const wrapper = document.createElement('div');
    const id = `$page-render_${vm.activeTab}`;
    wrapper.className = 'chart-wrap--full';
    wrapper.innerHTML = `
    <h2 class="chart-header">Bundle Size</h2>
    <div><canvas id=${id} width="400" height="100"></canvas></div>`;

    const chartsContainer = document.getElementById(vm.activeTab);
    chartsContainer.appendChild(wrapper);

    let labels = null;
    const datasets = Object.keys(data).reduce((acc, curr) => {

      if (!labels) {
        labels = data[curr].labels
      }
      return [
        ...acc,
        {
          label: `Bundle size ${curr}`,
          data: data[curr].points,
          borderColor: [
            bundlesColors[curr],
          ],
          backgroundColor: [
            'rgba(255, 99, 132, 0)',
          ],
          borderWidth: 2,
          pointHitRadius: 10,
          pointBorderColor: 'rgba(0, 84, 87, 0)',
          pointHoverBackgroundColor: color[curr],
          cubicInterpolationMode: 'monotone'
        }
      ]
    }, [])
    activeCharts.push(
      new Chart(document.getElementById(id), {
        type: 'line',
        data: {
          labels,
          datasets
        },
        spanGaps: true
      })
    )
  }

  const queryMap = {
    speedIndex: 'speed_index',
    avgPageSize: 'html_bytes,image_bytes,javascript_bytes,css_bytes,video_bytes,other_bytes',
    renderTime: 'mark.Page+Rendered',
  };

  function callApi(start, end, activeTab) {
    if (activeTab === 'bundleSize') {
      return fetch(`api/rigorChecks?start=${start}&end=${end}`).then(response => response.json())
    }

    return fetch(`api/rigorKPIs?start=${start}&end=${end}&metrics=${queryMap[activeTab]}`).then(response => response.json())
  }

  function renderCharts(json, type) {
    activeCharts.forEach(chart => chart.destroy());
    activeCharts = [];
    const data = populateDataMap[type](json);

    const chartsContainer = document.getElementById(vm.activeTab);
    chartsContainer.innerHTML = '';

    if (type === 'renderTime') {
      createPageRenderChart(data)
      return
    }

    if (type === 'bundleSize') {
      createBundleSizeChart(data)
      return
    }

    Object.keys(data).forEach((pageUrl) => {
      createChart(pageUrl, data)
    })
  }

  let activeCharts = [];

  const populateDataMap = {
    speedIndex: populateSpeedIndex,
    avgPageSize: populatePageSize,
    renderTime: populatePageRender,
    bundleSize: populateBundleSize
  }

  const tabs = document.querySelectorAll('[data-toggle="collapse"]')
  tabs.forEach(tab => {
    tab.addEventListener('click', function(e) {

      tabs.forEach(tab => {
        document.querySelector(tab.getAttribute('data-target')).style.display = 'none';
        tab.classList.remove('active')
      });

      e.target.classList.toggle('active');
      vm.activeTab = e.target.getAttribute('data-name');

      const panel = document.querySelector(e.target.getAttribute('data-target'));
      if (window.getComputedStyle(panel).display === 'block') {
        panel.style.display = 'none';
        return;
      }
      panel.style.display = 'block';

      callApi(
        vm.start.format('YYYY-MM-DD'),
        vm.end.format('YYYY-MM-DD'),
        vm.activeTab,
      ).then(json => renderCharts(json, vm.activeTab))

    })
  })

  $('input[name="dates"]').daterangepicker(
    {
      startDate: moment().add(-2, 'day'),
      endDate: moment(),
    },
    (start, end) => {
      vm.start = start;
      vm.end = end;
      callApi(
        vm.start.format('YYYY-MM-DD'),
        vm.end.format('YYYY-MM-DD'),
        vm.activeTab
      )
      .then(json => renderCharts(json, vm.activeTab))
      .catch(err => console.log(err))
    }

  );


  function addScript(src) {
    var elem = document.createElement("script");
    elem.src = src;
    document.head.appendChild(elem);
  }

  addScript('https://monitoring-api.rigor.com/v2/checks/real_browsers/96274/performance_kpis/data?range=last_12_hours&metrics[]=image_files');

}())

